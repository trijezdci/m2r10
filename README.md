## Modula-2 Revision 2010 ##

This project has been moved to Github.

### Project Page ###

https://github.com/m2sf

### Language Description ###

https://github.com/m2sf/m2bsk/wiki/Language-Description

### Syntax Diagrams ###

https://github.com/m2sf/M2-R10-Syntax-Diagrams/wiki/Syntax-Diagrams

### Bootstrap Kit ###

Modula-2 (PIM/ISO) hosted

https://github.com/m2sf/m2bsk

### Java hosted Kit ###

https://github.com/m2sf/m2j

### C# hosted Kit ###

https://github.com/m2sf/m2sharp